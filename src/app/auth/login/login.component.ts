import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "./../../models/user";
import { UserService } from "./../../services/user.service";

import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [UserService],
})
export class LoginComponent implements OnInit {
  public page_title: string;
  public page_subtitle: string;
  public user: User;
  public message_toast: string;
  public token;
  public identity;
  public alertsDismiss: any = [];
  public disabledBtn: boolean = false;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private toastr: ToastrService
  ) {
    this.page_title = "Identificate";
    this.page_subtitle = "para comenzar a vender !!!";

    this.user = new User(1, "", "", "", "", "VENDEDOR", 1, "");
  }

  ngOnInit(): void {}

  onSubmit(form) {
    //* SE DESACTIVA BOTÓN DE LOGIN
    this.disabledBtn = true;

    this._userService.singup(this.user).subscribe(
      (response) => {
        //* TOKEN
        if (response.status == "error") {
          this.message_toast = response.message; //Revisa tu e-mail o usuario.

          //* SE ENVIA MENSAJE DE ERROR A LA VISTA
          this.showError(this.message_toast);
        } else {
          this.token = response; //"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImVtYWlsIjox..."

          //* OBJETO USUARIO IDENTIFICADO
          this._userService.singup(this.user, true).subscribe(
            (response) => {
              this.identity = response;

              //* PERSISTIR DATOS DEL USUARIO
              localStorage.setItem("token", this.token);
              localStorage.setItem("identity", JSON.stringify(this.identity));

              //* REDIRECCIÓN A INICIO
              this._router.navigate(["/"]);
            },
            (err) => {
              console.log(<any>err);
            }
          );
        }

        //* SE HABILITA BOTÓN DE LOGIN
        this.disabledBtn = false;
      },
      (err) => {
        this.message_toast = err.error.message; //El usuario no se ha podido identificar

        /**------------------------------------------------------------------------
         *                           ERROR BACKEND
         *------------------------------------------------------------------------**/

        if (err.error.errors.email) {
          this.message_toast = err.error.errors.email; //required, type mail
        }

        if (err.error.errors.password) {
          this.message_toast = err.error.errors.password; //required
        }
        /*---------------------------- END OF ERROR BACKEND ----------------------------*/

        //* SE ENVIA MENSAJE DE ERROR A LA VISTA
        this.showError(this.message_toast);

        //* SE HABILITA BOTÓN DE LOGIN
        this.disabledBtn = false;
      }
    );
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }
}
