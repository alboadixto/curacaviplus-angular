import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { UserService } from "./../../services/user.service";

import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"],
  providers: [UserService],
})
export class ForgotPasswordComponent implements OnInit {
  public page_title: string;
  public page_subtitle: string;
  public message_toast: string;
  public disabledBtn: boolean = false;
  public emailValue: string;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private toastr: ToastrService
  ) {
    this.page_title = "Olvidaste la contraseña?";
    this.page_subtitle = "no te preocupes, ingresa el email";
    this.emailValue = "";
  }

  ngOnInit(): void {}

  onSubmit(form) {
    //* SE DESACTIVA BOTÓN DE LOGIN
    this.disabledBtn = true;

    this._userService.forgotPassword(this.emailValue).subscribe(
      (response) => {
        this.message_toast = response.message; //Correo enviado a (correo).

        this.showSuccess(this.message_toast);

        //* REDIRECCIÓN A INICIO
        this._router.navigate(["login"]);

        //* SE HABILITA BOTÓN DE LOGIN
        this.disabledBtn = false;
      },
      (err) => {
        this.message_toast = err.error.errors.email; //unique, max:45.

        this.showError(this.message_toast);

        //* SE HABILITA BOTÓN DE LOGIN
        this.disabledBtn = false;
      }
    );
  }

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }
}
