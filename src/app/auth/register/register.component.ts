import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "./../../models/user";
import { UserService } from "./../../services/user.service";

import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
  providers: [UserService],
})
export class RegisterComponent implements OnInit {
  public page_title: string;
  public page_subtitle: string;
  public user: User;
  public message_toast: string;
  public alertsDismiss: any = [];
  public disabledBtn: boolean = false;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private toastr: ToastrService
  ) {
    this.page_title = "Completa los datos";
    this.page_subtitle = "y comienza a vender de inmediato !!!";

    this.user = new User(1, "", "", "", "", "VENDEDOR", 1, "");
  }

  ngOnInit(): void {}

  onSubmit(form) {
    //* SE DESACTIVA BOTÓN DE REGISTRO
    this.disabledBtn = true;

    this._userService.register(this.user).subscribe(
      (response) => {
        this.message_toast = response.message; //El usuario se ha creado correctamente.
        this.showSuccess(this.message_toast);

        //* REDIRECCIÓN A INICIO
        this._router.navigate(["/login"]);

        form.reset();
      },
      (err) => {
        /**------------------------------------------------------------------------
         *                           ERROR BACKEND
         *------------------------------------------------------------------------**/
        if (err.error.errors.name) {
          this.message_toast = err.error.errors.name; //solo letras, max:45.
        }

        if (err.error.errors.email) {
          this.message_toast = err.error.errors.email; //unique, max:45.
        }

        if (err.error.errors.password) {
          this.message_toast = err.error.errors.password; //max:255.
        }
        /*---------------------------- END OF ERROR BACKEND ----------------------------*/

        //* SE ENVIA MENSAJE DE ERROR A LA VISTA
        /* this.closeAlerts();
        this.addAlertsDismiss("alert alert-fill-danger"); */
        this.showError(this.message_toast);

        //* SE HABILITA BOTÓN DE REGISTRO
        this.disabledBtn = false;
      }
    );
  }

  /*   addAlertsDismiss(type): void {
    this.alertsDismiss.push({
      type: type,
      timeout: 5000,
    });
  }

  closeAlerts() {
    this.alertsDismiss.splice(0);
  } */

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }
}
