import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Observable, Observer } from "rxjs";

import { UserService } from "../services/user.service";

import { environment } from "./../../environments/environment";

//* Import 3rd party components
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
/**--------------------------------------------
 *               NG-ZORRO
 *---------------------------------------------**/
import { NzUploadFile } from "ng-zorro-antd/upload";
import { ParseSourceSpan } from "@angular/compiler";

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
/*--------------- END OF NG-ZORRO --------------*/
@Component({
  selector: "app-user-edit",
  templateUrl: "./user-edit.component.html",
  styleUrls: ["./user-edit.component.scss"],
  providers: [UserService],
})
export class UserEditComponent implements OnInit {
  userForm!: FormGroup;
  panels = [
    {
      active: false,
      disabled: false,
      name: "Cambiar Contraseña",
      customStyle: {
        background: "#f7f7f7",
        "border-radius": "4px",
        "margin-bottom": "24px",
        "margin-top": "36px",
        border: "0px",
      },
    },
  ];
  public page_title: string;
  public page_subtitle: string;
  public message_toast: string;
  public token;
  public identity;
  public alertsDismiss: any = [];
  public disabledBtn: boolean = false;
  public url;
  public setHeaders;
  public setAction;
  public nzUploadFileConfig;
  public fileList: NzUploadFile[];
  public previewImage: string | undefined = "";
  public previewVisible = false;
  public newImage: string;

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.userForm.controls.checkPassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!this.userForm) {
      return {};
    } else if (
      this.userForm.controls.checkPassword.value !==
      this.userForm.controls.password.value
    ) {
      return { confirm: true, error: true };
    }
    return {};
  };

  constructor(
    private _userService: UserService,
    private toastr: ToastrService,
    private _fb: FormBuilder
  ) {
    this.page_title = "Ajustes de usuario";
    this.page_subtitle = "Modifica tus datos personales";

    this.identity = _userService.getIdentity();
    this.token = _userService.getToken();

    this.url = environment.baseUrl;
  }

  ngOnInit(): void {
    this.fileList = [];

    this.nzUploadFileConfig = {
      headers: {
        Authorization: this.token,
      },
      url: this.url + "user/upload",
    };

    this.buildEditForm();

    this.userForm.patchValue({
      id: this.identity.sub,
      img: this.identity.img,
      name: this.identity.name,
      email: this.identity.email,
    });
  }

  buildEditForm() {
    this.userForm = this._fb.group({
      id: [null],
      img: [null],
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(45),
          Validators.pattern("[a-zA-Z ]+"),
        ],
      ],
      email: [null, [Validators.email, Validators.required]],
      password: ["", [Validators.minLength(4)]],
      checkPassword: ["", [this.confirmationValidator]],
      currentPassword: [null],
    });
  }

  async onSubmit(form) {
    const { value: inputPassword } = await Swal.fire({
      title: "Contraseña actual",
      input: "password",
      inputPlaceholder: "",
      showCancelButton: true,
      confirmButtonText: "Cambiar!",
      confirmButtonColor: "#B66CFF",
      cancelButtonText: "Cancelar",
      cancelButtonColor: "#FF7C96",
      timer: 15000,
      timerProgressBar: true,
    });

    if (inputPassword) {
      this.userForm.value.currentPassword = inputPassword;

      //* SE DESACTIVA BOTÓN DE ACTUALIZAR
      this.disabledBtn = true;

      if (this.newImage) {
        this.userForm.value.img = this.newImage;
      }

      this._userService.update(this.token, form).subscribe(
        (response) => {
          this.message_toast = response.message; //El usuario se ha actualizado.

          //* ACTUALIZAR LOS DATOS DEL USUARIO EN LOCALSTORAGE
          this.identity.name = form.name;
          this.identity.email = form.email;
          this.identity.img = form.img;
          localStorage.setItem("identity", JSON.stringify(this.identity));

          //* SE ENVIA MENSAJE DE ACTUALIZACIÓN A LA VISTA
          this.showSuccess(this.message_toast);

          //* SE HABILITA BOTÓN DE ACTUALIZAR
          this.disabledBtn = false;
        },
        (err) => {
          if (err.error.status === "errorPass") {
            this.message_toast = err.error.message; //La contraseña es incorrecta.
          } else {
            if (err.error.errors.name) {
              this.message_toast = err.error.errors.name; //solo letras, max:45.
            }

            if (err.error.errors.email) {
              this.message_toast = err.error.errors.email; //unique, max:45.
            }
          }

          //* SE ENVIA MENSAJE DE ERROR A LA VISTA
          this.showError(this.message_toast);

          //* SE HABILITA BOTÓN DE ACTUALIZAR
          this.disabledBtn = false;
        }
      );
    }
  }

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }

  /* UPLOAD IMAGE */
  handleChange({ file, fileList }): void {
    const status = file.status;

    if (status === "error") {
      this.showError(`${file.name} falló al subir.`);
    }
    if (status === "done") {
      this.newImage = file.response.image;
    } else if (status === "removed") {
      this.newImage = this.identity.img;
    }
    if (status !== "uploading") {
      //console.log(file, fileList);
    }
  }

  //* ABRIR MODAL PARA VER IMAGEN EN GRANDE - (NO APLICA)
  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  beforeFileUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === "image/jpeg" ||
        file.type === "image/png" ||
        file.type === "image/jpg";
      if (!isJpgOrPng) {
        this.showError("Formatos soportados: .png, .jpeg y .jpg");
        observer.complete();
        return;
      }
      const isLt10M = file.size! / 1024 / 1024 < 10;
      if (!isLt10M) {
        this.showError(
          `${file.name} excede el tamaño máximo permitido de 10 MB.`
        );
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt10M);
      observer.complete();
    });
  };
  /* END UPLOAD IMAGE */
}
