import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS } from "@angular/common/http";

//* Import 3rd party components
import { AlertModule } from "ngx-bootstrap/alert";
import { NgxSpinnerModule } from "ngx-spinner";
/**--------------------------------------------
 *               NG-ZORRO
 * https://ng.ant.design/components/upload/en
 *---------------------------------------------**/
import { NZ_ICONS } from "ng-zorro-antd/icon";
import { IconDefinition } from "@ant-design/icons-angular";
import { NzIconModule } from "ng-zorro-antd/icon";
import { ngZorroAntdModule } from "./../ng-zorro-antd.module";

//* Import what you need. RECOMMENDED. ✔️
import {
  AccountBookFill,
  AlertFill,
  AlertOutline,
} from "@ant-design/icons-angular/icons";

const icons: IconDefinition[] = [AccountBookFill, AlertOutline, AlertFill];
/*--------------- END OF NG-ZORRO --------------*/

import { UserRoutingModule } from "./user-routing.module";

import { UserEditComponent } from "./user-edit.component";

import { InterceptorService } from "../services/interceptor.service";

@NgModule({
  declarations: [UserEditComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    AlertModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    ngZorroAntdModule,
    NzIconModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
    { provide: NZ_ICONS, useValue: icons },
  ],
  bootstrap: [UserEditComponent],
})
export class UserModule {}
