import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { UserEditComponent } from "./user-edit.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "User",
    },
    children: [
      {
        path: "",
        redirectTo: "ajustes",
      },
      {
        path: "ajustes",
        component: UserEditComponent,
        data: {
          title: "Ajustes",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
