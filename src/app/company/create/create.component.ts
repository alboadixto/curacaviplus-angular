import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Observer } from "rxjs";

import { Company } from "../../models/company";

import { CompanyService } from "../../services/company.service";
import { UserService } from "../../services/user.service";
import { UtilService } from "../../services/utils.service";

import { environment } from "./../../../environments/environment";

//* Import 3rd party components
import { ToastrService } from "ngx-toastr";
/**--------------------------------------------
 *               NG-ZORRO
 *---------------------------------------------**/
import { NzUploadFile } from "ng-zorro-antd/upload";

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
/*--------------- END OF NG-ZORRO --------------*/

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.scss"],
  providers: [UserService, CompanyService],
})
export class CreateComponent implements OnInit {
  public page_title: string;
  public page_subtitle: string;
  public token;
  public message_toast: string;
  public company: Company;
  public disabledBtn: boolean = false;
  public btnName: string;
  public url;
  public setHeaders;
  public setAction;
  public nzUploadFileConfig;
  public fileList: NzUploadFile[];
  public previewImage: string | undefined = "";
  public previewVisible = false;

  constructor(
    private toastr: ToastrService,
    private _companyService: CompanyService,
    private _router: Router,
    private _userService: UserService,
    public _util: UtilService
  ) {
    this.page_title = "Crear nuevo negocio";
    this.page_subtitle = "Ingresa los datos de tu negocio";
    this.btnName = "Guardar";
    this.company = new Company();
    this.url = environment.baseUrl;
    this.token = _userService.getToken();
  }

  ngOnInit(): void {
    this.fileList = [];

    this.nzUploadFileConfig = {
      headers: {
        Authorization: this.token,
      },
      url: this.url + "company/upload",
    };
  }

  onSubmit(form) {
    //* SE DESACTIVA BOTÓN DE ACTUALIZAR
    this.disabledBtn = true;

    this._companyService.create(this.token, this.company).subscribe(
      (response) => {
        this.message_toast = response.message; //El negocio se ha creado correctamente.

        this.company = response.company;

        //* REDIRECCIÓN A LA LISTA DE NEGOCIOS YA CREADOS
        this._router.navigate(["/company"]);

        //* SE ENVIA MENSAJE DE CREACIÓN A LA VISTA
        this.showSuccess(this.message_toast);

        //* SE HABILITA BOTÓN DE ACTUALIZAR
        this.disabledBtn = false;
      },
      (err) => {
        console.log(err.error);
        if (err.error.code == 400) {
          this.message_toast = err.error.message; //Los datos enviados no son correctos.
        } else {
          if (err.error.errors.name) {
            this.message_toast = err.error.errors.name; //required|max:45.
          } else if (err.error.errors.description) {
            this.message_toast = err.error.errors.description; //max:2000.
          } else if (err.error.errors.image) {
            this.message_toast = err.error.errors.image; //required.
          } else if (err.error.errors.calle) {
            this.message_toast = err.error.errors.calle; //required|max:45.
          } else if (err.error.errors.numero) {
            this.message_toast = err.error.errors.numero; //required|numeric.
          } else if (err.error.errors.villa) {
            this.message_toast = err.error.errors.villa; //max:45.
          } else if (err.error.errors.referencia) {
            this.message_toast = err.error.errors.referencia; //max:45.
          } else if (err.error.errors.phone) {
            this.message_toast = err.error.errors.phone; //required|min:9|max:9.
          } else if (err.error.errors.horario_ini) {
            this.message_toast = err.error.errors.horario_ini; //rrequired|date_format:H:i.
          } else if (err.error.errors.horario_fin) {
            this.message_toast = err.error.errors.horario_fin; //required|date_format:H:i|after:horario_ini.
          } else if (err.error.errors.valor_envio) {
            this.message_toast = err.error.errors.valor_envio; //required|integer.
          }
        }

        //* SE ENVIA MENSAJE DE ERROR A LA VISTA
        this.showError(this.message_toast);

        //* SE HABILITA BOTÓN DE ACTUALIZAR
        this.disabledBtn = false;
      }
    );
  }

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }

  /**--------------------------------------------
   *               UPLOAD FILE
   *---------------------------------------------**/
  handleChange({ file, fileList }): void {
    const status = file.status;

    if (status === "error") {
      this.showError(`${file.name} falló al subir.`);
    }
    if (status === "done") {
      this.company.image = file.response.image;
    } else if (status === "removed") {
      this.company.image = "";
    }
    if (status !== "uploading") {
      //console.log(file, fileList);
    }
  }

  //* ABRIR MODAL PARA VER IMAGEN EN GRANDENO - (NO APLICA)
  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  beforeFileUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === "image/jpeg" ||
        file.type === "image/png" ||
        file.type === "image/jpg";
      if (!isJpgOrPng) {
        this.showError("Formatos soportados: .png, .jpeg y .jpg");
        observer.complete();
        return;
      }
      const isLt10M = file.size! / 1024 / 1024 < 10;
      if (!isLt10M) {
        this.showError(
          `${file.name} excede el tamaño máximo permitido de 10 MB.`
        );
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt10M);
      observer.complete();
    });
  };
  /*--------------- END OF UPLOAD FILE --------------*/
}
