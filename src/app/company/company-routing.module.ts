import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { IndexComponent } from "./index/index.component";
import { CreateComponent } from "./create/create.component";
import { ProductCreateComponent } from "./product-create/product-create.component";

const routes: Routes = [
  {
    path: "",
    children: [
      { path: "", redirectTo: "index" },
      { path: "index", component: IndexComponent },
      { path: "create", component: CreateComponent },
      { path: "product/create", component: ProductCreateComponent },
      { path: "**", redirectTo: "index" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompanyRoutingModule {}
