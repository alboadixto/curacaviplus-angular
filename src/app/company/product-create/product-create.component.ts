import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Observer } from "rxjs";

import { Product } from "../../models/product";

import { ProductService } from "../../services/product.service";
import { UserService } from "../../services/user.service";
import { UtilService } from "../../services/utils.service";

import { environment } from "./../../../environments/environment";

//* Import 3rd party components
import { ToastrService } from "ngx-toastr";
/**--------------------------------------------
 *               NG-ZORRO
 *---------------------------------------------**/
import { NzUploadFile } from "ng-zorro-antd/upload";
import { isNull } from "util";

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
/*--------------- END OF NG-ZORRO --------------*/

@Component({
  selector: "app-product-create",
  templateUrl: "./product-create.component.html",
  styleUrls: ["./product-create.component.scss"],
  providers: [UserService, ProductService],
})
export class ProductCreateComponent implements OnInit {
  @Output() newProduct = new EventEmitter();

  public token;
  public message_toast: string;
  public product: Product;
  public disabledBtn: boolean = false;
  public btnName: string;
  public url;
  public companyId: number;
  public setHeaders;
  public setAction;
  public nzUploadFileConfig;
  public fileList: NzUploadFile[];
  public previewImage: string | undefined = "";
  public previewVisible = false;

  constructor(
    private toastr: ToastrService,
    private _productService: ProductService,
    private _router: Router,
    private _userService: UserService,
    public _util: UtilService
  ) {
    this.companyId = this._util.companyId;
    if (!this.companyId) {
      //* REDIRECCIÓN A LA VISTA DE COMPAÑIAS
      this._router.navigate(["/company"]);
    }
    this.product = new Product();
    this.btnName = "Guardar";
    this.url = environment.baseUrl;
    this.token = _userService.getToken();
  }

  ngOnInit(): void {
    this.fileList = [];

    this.nzUploadFileConfig = {
      headers: {
        Authorization: this.token,
      },
      url: this.url + "product/upload",
    };
  }

  onSubmit(form) {
    this.product.company_id = this.companyId;
    this.product.name = form.value.name;
    this.product.description = form.value.description;
    this.product.price = form.value.precio;
    this.product.stock = form.value.stock;

    //* SE HABILITA BOTÓN DE GUARDAR/ACTUALIZAR
    this.disabledBtn = true;

    this._productService.create(this.token, this.product).subscribe(
      (response) => {
        this.message_toast = response.message; //El producto se ha creado correctamente.

        //* ENVIO FUNCION AL COMPONENTE PADRE PARA RECARGAR LISTA DE PRODUCTOS
        this.newProduct.emit();

        //* SE ENVIA MENSAJE DE CREACIÓN A LA VISTA
        this.showSuccess(this.message_toast);

        //* SE HABILITA BOTÓN DE GUARDAR/ACTUALIZAR
        this.disabledBtn = false;
      },
      (err) => {
        if (err.error.code == 400) {
          this.message_toast = err.error.message; //Los datos enviados no son correctos.
        } else {
          if (err.error.errors.name) {
            this.message_toast = err.error.errors.name; //required|max:45.
          } else if (err.error.errors.description) {
            this.message_toast = err.error.errors.description; //max:2000.
          } else if (err.error.errors.price) {
            this.message_toast = err.error.errors.price; //required|numeric.
          } else if (err.error.errors.stock) {
            this.message_toast = err.error.errors.stock; //numeric.
          } else if (err.error.errors.company_id) {
            this.message_toast = err.error.errors.company_id; //required.
          } else if (err.error.errors.image) {
            this.message_toast = err.error.errors.image; //required.
          }
        }

        //* SE ENVIA MENSAJE DE ERROR A LA VISTA
        this.showError(this.message_toast);

        //* SE HABILITA BOTÓN DE GUARDAR/ACTUALIZAR
        this.disabledBtn = false;
      }
    );
  }

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }

  /**--------------------------------------------
   *               UPLOAD FILE
   *---------------------------------------------**/
  handleChange({ file, fileList }): void {
    const status = file.status;

    if (status === "error") {
      this.showError(`${file.name} falló al subir.`);
    }
    if (status === "done") {
      this.product.image = file.response.image;
    } else if (status === "removed") {
      this.product.image = "";
    }
    if (status !== "uploading") {
      //console.log(file, fileList);
    }
  }

  //* ABRIR MODAL PARA VER IMAGEN EN GRANDE - (NO APLICA)
  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  beforeFileUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === "image/jpeg" ||
        file.type === "image/png" ||
        file.type === "image/jpg";
      if (!isJpgOrPng) {
        this.showError("Formatos soportados: .png, .jpeg y .jpg");
        observer.complete();
        return;
      }
      const isLt10M = file.size! / 1024 / 1024 < 10;
      if (!isLt10M) {
        this.showError(
          `${file.name} excede el tamaño máximo permitido de 10 MB.`
        );
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt10M);
      observer.complete();
    });
  };
  /*--------------- END OF UPLOAD FILE --------------*/
}
