import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { Product } from "../../models/product";

import { UtilService } from "./../../services/utils.service";
@Component({
  selector: "app-product-index",
  templateUrl: "./product-index.component.html",
  styleUrls: ["./product-index.component.scss"],
})
export class ProductIndexComponent implements OnInit {
  public isVisibleEdit = false;
  public product: Product;
  public switchValue = false;

  @Input() company;
  @Input() url: string;
  @Input() loading: boolean;

  @Output() productDelete = new EventEmitter();
  @Output() clickSwitch = new EventEmitter();

  constructor(private _util: UtilService) {}

  ngOnInit(): void {
    this._util.isVisible$.subscribe((isVisible) => {
      this.isVisibleEdit = isVisible;
    });
  }

  deleteProduct(productId) {
    this.productDelete.emit(productId);
  }

  /**--------------------------------------------
   *               MODAL
   *---------------------------------------------**/
  showModalEdit(product: Product): void {
    this.product = product;
    this.isVisibleEdit = true;
  }

  handleCancel(): void {
    this.isVisibleEdit = false;
  }
  /*--------------- END OF MODAL --------------*/

  clickSwitchProduct(product) {
    this.clickSwitch.emit(product);
    this.product = product;

    this._util.checked$.subscribe((checked) => {
      this.product.is_active = checked;
      //console.log(this.product.is_active);
    });
  }
}
