import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS } from "@angular/common/http";

//* Import 3rd party components
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AlertModule } from "ngx-bootstrap/alert";
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxSummernoteModule } from "ngx-summernote";
/**--------------------------------------------
 *               NG-ZORRO
 * https://ng.ant.design/components/upload/en
 *---------------------------------------------**/
import { NZ_ICONS } from "ng-zorro-antd/icon";
import { IconDefinition } from "@ant-design/icons-angular";
import { NzIconModule } from "ng-zorro-antd/icon";
import { ngZorroAntdModule } from "./../ng-zorro-antd.module";

//* Import what you need. RECOMMENDED. ✔️
import {
  AccountBookFill,
  AlertFill,
  AlertOutline,
} from "@ant-design/icons-angular/icons";

const icons: IconDefinition[] = [AccountBookFill, AlertOutline, AlertFill];
/*--------------- END OF NG-ZORRO --------------*/

//* Services
import { InterceptorService } from "../services/interceptor.service";

import { CompanyRoutingModule } from "./company-routing.module";
import { IndexComponent } from "./index/index.component";
import { CreateComponent } from "./create/create.component";
import { FormComponent } from "./form/form.component";
import { ProductEditComponent } from "./product-edit/product-edit.component";
import { ProductCreateComponent } from "./product-create/product-create.component";
import { ProductFormComponent } from "./product-form/product-form.component";
import { ProductIndexComponent } from "./product-index/product-index.component";

@NgModule({
  declarations: [
    IndexComponent,
    CreateComponent,
    FormComponent,
    ProductEditComponent,
    ProductCreateComponent,
    ProductFormComponent,
    ProductIndexComponent,
  ],
  imports: [
    ngZorroAntdModule,
    NzIconModule,
    AlertModule,
    CommonModule,
    CompanyRoutingModule,
    FormsModule,
    NgbModule,
    NgxSpinnerModule,
    NgxSummernoteModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
    { provide: NZ_ICONS, useValue: icons },
  ],
})
export class CompanyModule {}
