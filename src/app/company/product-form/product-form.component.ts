import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";

import { UtilService } from "./../../services/utils.service";

@Component({
  selector: "app-product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.scss"],
})
export class ProductFormComponent implements OnInit {
  @Input() url;
  @Input() productDetail;
  @Input() summernote_options;
  @Input() btnName;
  @Input() disabledBtn;
  @Input() nzUploadFileConfig;
  @Input() handlePreview;
  @Input() beforeFileUpload;
  @Input() fileList;

  @Output() productSubmit = new EventEmitter();
  @Output() nzChange = new EventEmitter();

  constructor(private _util: UtilService) {}

  ngOnInit(): void {}

  closeModal() {
    this._util.getIsVisible(false);
  }

  onSubmit(form) {
    this.productSubmit.emit(form);
  }

  handleChange(event) {
    this.nzChange.emit(event);
  }
}
