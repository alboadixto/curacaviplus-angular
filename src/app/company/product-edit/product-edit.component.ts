import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Observer } from "rxjs";

import { Product } from "./../../models/product";

import { ProductService } from "../../services/product.service";
import { UserService } from "../../services/user.service";
import { UtilService } from "../../services/utils.service";

import { environment } from "./../../../environments/environment";

//* Import 3rd party components
import { ToastrService } from "ngx-toastr";
/**--------------------------------------------
 *               NG-ZORRO
 *---------------------------------------------**/
import { NzUploadFile } from "ng-zorro-antd/upload";

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
/*--------------- END OF NG-ZORRO --------------*/

@Component({
  selector: "app-product-edit",
  templateUrl: "./product-edit.component.html",
  styleUrls: ["./product-edit.component.scss"],
  providers: [UserService, ProductService],
})
export class ProductEditComponent implements OnInit {
  @Input() product;

  public productEdit: Product;
  public token: string;
  public message_toast: string;
  public disabledBtn: boolean = false;
  public btnName: string;
  public url: string;
  public nzUploadFileConfig: {};
  public fileList: NzUploadFile[];
  public previewImage: string | undefined = "";
  public previewVisible = false;

  constructor(
    private toastr: ToastrService,
    private _productService: ProductService,
    private _router: Router,
    private _userService: UserService,
    public _util: UtilService
  ) {
    this.productEdit = new Product();
    this.btnName = "Actualizar";
    this.url = environment.baseUrl;
    this.token = _userService.getToken();
  }

  ngOnInit(): void {
    this.fileList = [];

    this.nzUploadFileConfig = {
      headers: {
        Authorization: this.token,
      },
      url: this.url + "product/upload",
    };
  }

  onSubmit(form) {
    this.productEdit.name = form.value.name;
    this.productEdit.description = form.value.description;
    this.productEdit.price = form.value.precio;
    this.productEdit.stock = form.value.stock;
    if (!this.productEdit.image) {
      this.productEdit.image = this.product.image.url;
    }

    //* SE HABILITA BOTÓN DE GUARDAR/ACTUALIZAR
    this.disabledBtn = true;

    this._productService
      .update(this.token, this.productEdit, this.product.id)
      .subscribe(
        (response) => {
          this.message_toast = response.message; //El producto se ha actualizado.

          //* ACTUALIZAR VALORES DEL PRODUCTO PARA QUE SE MUESTREN LOS CAMBIOS
          this.product.name = this.productEdit.name;
          this.product.description = this.productEdit.description;
          this.product.price = this.productEdit.price;
          this.product.stock = this.productEdit.stock
            ? Number(this.productEdit.stock)
            : null;
          this.product.image.url = this.productEdit.image;

          //* ENVIO AL SERVICIO FALSE PARA QUE CIERRE EL MODAL
          this._util.getIsVisible(false);

          //* SE ENVIA MENSAJE DE CREACIÓN A LA VISTA
          this.showSuccess(this.message_toast);

          //* SE HABILITA BOTÓN DE GUARDAR/ACTUALIZAR
          this.disabledBtn = false;
        },
        (err) => {
          if (err.error.code == 400) {
            this.message_toast = err.error.message; //Los datos enviados no son correctos.
          } else {
            if (err.error.errors.name) {
              this.message_toast = err.error.errors.name; //required|max:45.
            } else if (err.error.errors.description) {
              this.message_toast = err.error.errors.description; //max:2000.
            } else if (err.error.errors.price) {
              this.message_toast = err.error.errors.price; //required|numeric.
            } else if (err.error.errors.stock) {
              this.message_toast = err.error.errors.stock; //numeric.
            } else if (err.error.errors.image) {
              this.message_toast = err.error.errors.image; //required.
            }
          }

          //* SE ENVIA MENSAJE DE ERROR A LA VISTA
          this.showError(this.message_toast);

          //* SE HABILITA BOTÓN DE GUARDAR/ACTUALIZAR
          this.disabledBtn = false;
        }
      );
  }

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }

  /**--------------------------------------------
   *               UPLOAD FILE
   *---------------------------------------------**/
  handleChange({ file, fileList }): void {
    const status = file.status;

    if (status === "error") {
      this.showError(`${file.name} falló al subir.`);
    }
    if (status === "done") {
      this.productEdit.image = file.response.image;
    } else if (status === "removed") {
      this.productEdit.image = "";
    }
    if (status !== "uploading") {
      //console.log(file, fileList);
    }
  }

  //* ABRIR MODAL PARA VER IMAGEN EN GRANDE - (NO APLICA)
  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  beforeFileUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === "image/jpeg" ||
        file.type === "image/png" ||
        file.type === "image/jpg";
      if (!isJpgOrPng) {
        this.showError("Formatos soportados: .png, .jpeg y .jpg");
        observer.complete();
        return;
      }
      const isLt10M = file.size! / 1024 / 1024 < 10;
      if (!isLt10M) {
        this.showError(
          `${file.name} excede el tamaño máximo permitido de 10 MB.`
        );
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt10M);
      observer.complete();
    });
  };
  /*--------------- END OF UPLOAD FILE --------------*/
}
