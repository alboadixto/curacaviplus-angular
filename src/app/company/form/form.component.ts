import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class FormComponent implements OnInit {
  @Input() url;
  @Input() company;
  @Input() summernote_options;
  @Input() btnName;
  @Input() disabledBtn;
  @Input() nzUploadFileConfig;
  @Input() handlePreview;
  @Input() beforeFileUpload;
  @Input() fileList;

  @Output() empresaSubmit = new EventEmitter();
  @Output() nzChange = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onSubmit(form) {
    this.empresaSubmit.emit(form);
  }

  handleChange(event) {
    this.nzChange.emit(event);
  }
}
