import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Observer } from "rxjs";

import { Company } from "../../models/company";

import { environment } from "../../../environments/environment";

import { CompanyService } from "../../services/company.service";
import { ProductService } from "../../services/product.service";
import { UserService } from "../../services/user.service";
import { UtilService } from "../../services/utils.service";

//* Import 3rd party components
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
/**--------------------------------------------
 *               UPLOAD FILE
 *---------------------------------------------**/
import { NzUploadFile } from "ng-zorro-antd/upload";

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
/*--------------- END OF UPLOAD FILE --------------*/

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"],
  providers: [UserService, CompanyService, ProductService],
})
export class IndexComponent implements OnInit {
  public page_title: string;
  public page_subtitle: string;
  public token: string;
  public title_toast: string;
  public message_toast: string;
  public company: Company;
  public disabledBtn = false;
  public btnName: string;
  public url: string;
  public companies: Array<Company>;
  public isVisibleCreate = false;
  public nzUploadFileConfig: {};
  public fileList: NzUploadFile[];
  public previewImage: string | undefined = "";
  public previewVisible = false;
  public newImage: string;
  public loading = false;
  public checked: number = null;

  constructor(
    private _companyService: CompanyService,
    private _router: Router,
    private _userService: UserService,
    public _util: UtilService,
    private toastr: ToastrService,
    private _productService: ProductService
  ) {
    this.page_title = "Lista de negocios";
    this.btnName = "Actualizar";
    this.company = new Company();
    this.url = environment.baseUrl;
    this.token = _userService.getToken();
  }

  ngOnInit(): void {
    this.getCompanies();

    this.fileList = [];

    this.nzUploadFileConfig = {
      headers: {
        Authorization: this.token,
      },
      url: this.url + "company/upload",
    };

    this._util.isVisible$.subscribe((isVisible) => {
      this.isVisibleCreate = isVisible;
    });
  }

  getCompanies() {
    this._companyService.getCompanies(this.token).subscribe(
      (response) => {
        this.companies = response.companies;
      },
      (err) => {
        //* REDIRECCIÓN AL LOGIN PARA VOLVER A INICAR SESIÓN
        this.logout();
      }
    );
  }

  onSubmit(form) {
    let data: any = this.companies.find((x) => x.id == form.value.id);
    //* COMPLETAR OBJETO CAMPAÑA
    this.company = new Company(
      form.value.id,
      form.value.name,
      form.value.description,
      this.newImage ? this.newImage : data.image.url,
      form.value.calle,
      form.value.numero,
      form.value.villa,
      form.value.referencia,
      form.value.phone,
      form.value.horario_ini,
      form.value.horario_fin,
      form.value.valor_envio
    );

    //* SE DESACTIVA BOTÓN DE ACTUALIZAR
    this.disabledBtn = true;

    this._companyService
      .update(this.token, this.company, this.company.id)
      .subscribe(
        (response) => {
          //* MODIFICO LAS COMPANIES CON EL NOMBRE DE IMAGEN QUE SE ACTUALIZÓ
          this.companies.map(function (dato: any) {
            if (dato.id == form.value.id) {
              dato.image.url = response.change.image;
            }
          });

          this.message_toast = response.message; //La empresa se ha creado correctamente.

          //* SE ENVIA MENSAJE DE CREACIÓN A LA VISTA
          this.showSuccess(this.message_toast);

          //* SE HABILITA BOTÓN DE ACTUALIZAR
          this.disabledBtn = false;

          //* ELIMINO EL ARCHIVO DE IMAGEN QUE SE SUBIO
          this.newImage = null;
        },
        (err) => {
          console.log(err.error);
          if (err.error.code == 400) {
            this.message_toast = err.error.message; //Los datos enviados no son correctos.
          } else {
            if (err.error.errors.name) {
              this.message_toast = err.error.errors.name; //required|max:45.
            } else if (err.error.errors.description) {
              this.message_toast = err.error.errors.description; //max:2000.
            } else if (err.error.errors.image) {
              this.message_toast = err.error.errors.image; //required.
            } else if (err.error.errors.calle) {
              this.message_toast = err.error.errors.calle; //required|max:45.
            } else if (err.error.errors.numero) {
              this.message_toast = err.error.errors.numero; //required|numeric.
            } else if (err.error.errors.villa) {
              this.message_toast = err.error.errors.villa; //max:45.
            } else if (err.error.errors.referencia) {
              this.message_toast = err.error.errors.referencia; //max:45.
            } else if (err.error.errors.phone) {
              this.message_toast = err.error.errors.phone; //required|min:9|max:9.
            } else if (err.error.errors.horario_ini) {
              this.message_toast = err.error.errors.horario_ini; //rrequired.
            } else if (err.error.errors.horario_fin) {
              this.message_toast = err.error.errors.horario_fin; //required|after:horario_ini.
            } else if (err.error.errors.valor_envio) {
              this.message_toast = err.error.errors.valor_envio; //required|integer.
            }
          }

          //* SE ENVIA MENSAJE DE ERROR A LA VISTA
          this.showError(this.message_toast);

          //* SE HABILITA BOTÓN DE ACTUALIZAR
          this.disabledBtn = false;
        }
      );
  }

  showSuccess(message_toast) {
    this.toastr.success(message_toast);
  }

  showError(message_toast) {
    this.toastr.error(message_toast);
  }

  async deleteCompany(companyId) {
    const { value: accept } = await Swal.fire({
      title: "Estas seguro?",
      text: "Esta acción no se podrá deshacer!",
      icon: "warning",
      input: "checkbox",
      inputValue: 0,
      inputPlaceholder: "Si, estoy seguro!",
      showCancelButton: true,
      confirmButtonText: "Elimínalo!",
      confirmButtonColor: "#B66CFF",
      cancelButtonText: "Cancelar",
      cancelButtonColor: "#FF7C96",
      timer: 15000,
      timerProgressBar: true,
      inputValidator: (result) => {
        return !result && "Necesitas estar seguro...";
      },
    });

    if (accept) {
      this._companyService.delete(this.token, companyId).subscribe(
        (response: any) => {
          this.getCompanies();

          Swal.fire(response.message, "", "success");
        },
        (err) => {
          console.log(err.error);

          Swal.fire("Hubo un error!", err.error.message, "error");
        }
      );
    }
  }

  deleteProduct(productId) {
    Swal.fire({
      title: "Estas seguro?",
      text: "Esta acción no se podrá deshacer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Elimínalo!",
      confirmButtonColor: "#B66CFF",
      cancelButtonText: "Cancelar",
      cancelButtonColor: "#FF7C96",
      timer: 15000,
      timerProgressBar: true,
    }).then((result) => {
      if (result.isConfirmed) {
        this._productService.delete(this.token, productId).subscribe(
          (response: any) => {
            this.getCompanies();

            Swal.fire(response.message, "", "success");
          },
          (err) => {
            console.log(err.error);

            Swal.fire("Hubo un error!", err.error.message, "error");
          }
        );
      }
    });
  }

  refreshCompanies() {
    this.getCompanies();
    this.isVisibleCreate = false;
  }

  /**--------------------------------------------
   *               MODAL
   *---------------------------------------------**/
  showModalCreate(companyId: number): void {
    this._util.getCompanyId(companyId);
    this.isVisibleCreate = true;
  }

  handleCancel(): void {
    this.isVisibleCreate = false;
  }
  /*--------------- END OF MODAL --------------*/

  /**--------------------------------------------
   *               UPLOAD FILE
   *---------------------------------------------**/
  handleChange({ file, fileList }): void {
    const status = file.status;

    if (status === "error") {
      this.showError(`${file.name} falló al subir.`);
    }
    if (status === "done") {
      this.newImage = file.response.image;
    } else if (status === "removed") {
      this.newImage = "";
    }
    if (status !== "uploading") {
      //console.log(file, fileList);
    }
  }

  //* ABRIR MODAL PARA VER IMAGEN EN GRANDE - (NO APLICA)
  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  beforeFileUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === "image/jpeg" ||
        file.type === "image/png" ||
        file.type === "image/jpg";
      if (!isJpgOrPng) {
        this.showError("Formatos soportados: .png, .jpeg y .jpg");
        observer.complete();
        return;
      }
      const isLt10M = file.size! / 1024 / 1024 < 10;
      if (!isLt10M) {
        this.showError(
          `${file.name} excede el tamaño máximo permitido de 10 MB.`
        );
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt10M);
      observer.complete();
    });
  };
  /*--------------- END OF UPLOAD FILE --------------*/

  clickSwitch(companyId: number, company_is_active: number) {
    if (!this.loading) {
      this.loading = true;

      let value = 0;
      if (company_is_active == 0) {
        value = 1;
      }

      this._companyService.is_active(this.token, value, companyId).subscribe(
        (response) => {
          this.loading = false;

          //* MODIFICO EL ARRAY - CAMPO (IS_ACTIVE) DE LA COMPANY
          this.companies.map(function (dato) {
            if (dato.id == companyId) {
              dato.is_active = value;
            }
          });
        },
        (err) => {
          this.loading = false;

          console.log(err.error);

          this.message_toast = err.error.message; //Los datos enviados no son correctos.

          //* SE ENVIA MENSAJE DE ERROR A LA VISTA
          this.showError(this.message_toast);
        }
      );
    }
  }

  clickSwitchProduct(product) {
    if (!this.loading) {
      this.loading = true;

      let value = 0;
      if (product["is_active"] == 0) {
        value = 1;
      }

      this._productService
        .is_active(this.token, value, product["id"])
        .subscribe(
          (response) => {
            this.loading = false;

            this._util.getChecked(value);
          },
          (err) => {
            this.loading = false;

            console.log(err.error);

            this.message_toast = err.error.message; //Los datos enviados no son correctos.

            //* SE ENVIA MENSAJE DE ERROR A LA VISTA
            this.showError(this.message_toast);
          }
        );
    }
  }

  logout() {
    localStorage.clear();

    //* REDIRECCIÓN A INICIO
    this._router.navigate(["/login"]);
  }
}
