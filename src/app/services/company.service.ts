import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable()
export class CompanyService {
  public url: string;

  constructor(public _http: HttpClient) {
    this.url = environment.baseUrl; //https://laravel.curacaviplus.cl/api/
  }

  create(token, company): Observable<any> {
    //* LIMPIAR CAMPO CONTENT (FROALA) HTMLENTITIES > UFT8
    company.description = environment.htmlEntities(company.description);

    let json = JSON.stringify(company);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.post(this.url + "company", params, { headers: headers });
  }

  getCompanies(token: string): Observable<any> {
    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.get(this.url + "company", { headers: headers });
  }

  update(token, company, id): Observable<any> {
    //* LIMPIAR CAMPO CONTENT (FROALA) HTMLENTITIES > UFT8
    company.description = environment.htmlEntities(company.description);

    let json = JSON.stringify(company);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.put(this.url + "company/" + id, params, {
      headers: headers,
    });
  }

  delete(token, id) {
    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.delete(this.url + "company/" + id, { headers: headers });
  }

  is_active(token, value, id): Observable<any> {
    let json = JSON.stringify(value);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.put(this.url + "company/is_active/" + id, params, {
      headers: headers,
    });
  }
}
