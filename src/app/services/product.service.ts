import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable()
export class ProductService {
  public url: string;

  constructor(public _http: HttpClient) {
    this.url = environment.baseUrl; //https://laravel.curacaviplus.cl/api/
  }

  create(token, product): Observable<any> {
    //* LIMPIAR CAMPO CONTENT (FROALA) HTMLENTITIES > UFT8
    product.description = environment.htmlEntities(product.description);

    let json = JSON.stringify(product);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.post(this.url + "product", params, { headers: headers });
  }

  update(token, product, id): Observable<any> {
    //* LIMPIAR CAMPO CONTENT (FROALA) HTMLENTITIES > UFT8
    product.description = environment.htmlEntities(product.description);

    let json = JSON.stringify(product);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.put(this.url + "product/" + id, params, {
      headers: headers,
    });
  }

  delete(token, id) {
    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.delete(this.url + "product/" + id, { headers: headers });
  }

  is_active(token, value, id): Observable<any> {
    let json = JSON.stringify(value);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.put(this.url + "product/is_active/" + id, params, {
      headers: headers,
    });
  }
}
