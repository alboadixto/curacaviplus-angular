import { Injectable } from "@angular/core";
import { CanActivate, Router, CanLoad } from "@angular/router";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root",
})
export class IdentityGuard implements CanActivate, CanLoad {
  constructor(private _router: Router, private _userService: UserService) {}

  canActivate() {
    return this.checkLogin();
  }

  canLoad() {
    return this.checkLogin();
  }

  checkLogin() {
    let identity = this._userService.getIdentity();

    if (identity) {
      return true;
    } else {
      this._router.navigate(["/login"]);
      return false;
    }
  }
}
