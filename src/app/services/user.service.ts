import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class UserService {
  public url: string;
  public identity;
  public token;
  constructor(public _http: HttpClient) {
    this.url = environment.baseUrl; //https://laravel.curacaviplus.cl/api/
  }

  register(user): Observable<any> {
    let json = JSON.stringify(user);
    let params = "json=" + json;

    let headers = new HttpHeaders().set(
      "content-type",
      "application/x-www-form-urlencoded"
    );

    return this._http.post(this.url + "register", params, { headers: headers });
  }

  singup(user, getToken = null): Observable<any> {
    if (getToken != null) {
      user.getToken = true;
    }

    let json = JSON.stringify(user);
    let params = "json=" + json;

    let headers = new HttpHeaders().set(
      "content-type",
      "application/x-www-form-urlencoded"
    );

    return this._http.post(this.url + "login", params, { headers: headers });
  }

  update(token, user): Observable<any> {
    let json = JSON.stringify(user);
    let params = "json=" + json;

    let headers = new HttpHeaders()
      .set("content-type", "application/x-www-form-urlencoded")
      .set("authorization", token);

    return this._http.put(this.url + "user/update", params, {
      headers: headers,
    });
  }

  uploadImage(token, formData): Observable<any> {
    let json = JSON.stringify(formData);
    let params = "json=" + json;

    let headers = new HttpHeaders().set("authorization", token);

    return this._http.post(this.url + "user/upload", formData, {
      headers: headers,
    });
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem("identity"));

    if (!identity && identity == "undefined") {
      this.identity = null;
    } else {
      this.identity = identity;
    }

    return this.identity;
  }

  getToken() {
    let token = localStorage.getItem("token");

    if (!token && token == "undefined") {
      this.token = null;
    } else {
      this.token = token;
    }

    return this.token;
  }

  forgotPassword(email): Observable<any> {
    let json = JSON.stringify(email);
    let params = "json=" + json;

    let headers = new HttpHeaders().set(
      "content-type",
      "application/x-www-form-urlencoded"
    );

    return this._http.post(this.url + "forgot-password", params, {
      headers: headers,
    });
  }
}
