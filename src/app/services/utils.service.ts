import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UtilService {
  public summernote_options: any = {
    placeholder: "Ingrese una descripción aquí...",
    tabsize: 2,
    height: "200px",
    //uploadImagePath: "/api/upload",
    toolbar: [
      ["misc", ["undo", "redo"]],
      ["font", ["bold", "italic", "underline"]],
      ["fontsize", ["fontsize"]],
      ["para", ["ul", "ol"]],
    ],
    fontNames: [
      "Helvetica",
      "Arial",
      "Arial Black",
      "Comic Sans MS",
      "Courier New",
      "Roboto",
      "Times",
    ],
  };

  companyId: number;
  private companyIdSource = new Subject<number>();
  companyId$ = this.companyIdSource.asObservable();

  getCompanyId(companyId: number) {
    this.companyId = companyId;
    this.companyIdSource.next(companyId);
  }

  isVisible: boolean;
  private isVisibleSource = new Subject<boolean>();
  isVisible$ = this.isVisibleSource.asObservable();

  getIsVisible(isVisible: boolean) {
    this.isVisible = isVisible;
    this.isVisibleSource.next(isVisible);
  }

  checked: number;
  private checkedSource = new Subject<number>();
  checked$ = this.checkedSource.asObservable();

  getChecked(checked: number) {
    this.checked = checked;
    this.checkedSource.next(checked);
  }
}
