import { Injectable } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: "root",
})
export class SpinnerService {
  constructor(private _spinnerService: NgxSpinnerService) {}

  public startSpinner() {
    this._spinnerService.show("mySpinner", {
      type: "pacman",
      size: "medium",
      bdColor: "rgba(255,255,255, 0.5)",
      color: "#b66cff",
      fullScreen: true,
    });
  }
  public stopSpinner() {
    this._spinnerService.hide("mySpinner", 100);
  }
}
