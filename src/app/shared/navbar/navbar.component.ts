import { Component, OnInit, DoCheck } from "@angular/core";
import { NgbDropdownConfig } from "@ng-bootstrap/ng-bootstrap";
import { UserService } from "./../../services/user.service";
import { environment } from "./../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
  providers: [NgbDropdownConfig, UserService],
})
export class NavbarComponent implements OnInit, DoCheck {
  public identity;
  public token;

  public iconOnlyToggled = false;
  public sidebarToggled = false;

  public url;

  constructor(
    config: NgbDropdownConfig,
    public _userService: UserService,
    private _router: Router
  ) {
    config.placement = "bottom-right";
    this.loadUser();

    this.url = environment.baseUrl;
  }

  ngOnInit() {
    //* AL INICIAR LA PÁGINA, SE COLAPSE EL SIDEBAR
    this.toggleSidebar();
  }

  // toggle sidebar in small devices
  toggleOffcanvas() {
    document.querySelector(".sidebar-offcanvas").classList.toggle("active");
  }

  // toggle sidebar
  toggleSidebar() {
    let body = document.querySelector("body");
    if (
      !body.classList.contains("sidebar-toggle-display") &&
      !body.classList.contains("sidebar-absolute")
    ) {
      this.iconOnlyToggled = !this.iconOnlyToggled;
      if (this.iconOnlyToggled) {
        body.classList.add("sidebar-icon-only");
      } else {
        body.classList.remove("sidebar-icon-only");
      }
    } else {
      this.sidebarToggled = !this.sidebarToggled;
      if (this.sidebarToggled) {
        body.classList.add("sidebar-hidden");
      } else {
        body.classList.remove("sidebar-hidden");
      }
    }
  }

  // toggle right sidebar
  // toggleRightSidebar() {
  //   document.querySelector('#right-sidebar').classList.toggle('open');
  // }

  ngDoCheck() {
    this.loadUser();
  }

  loadUser() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  logout() {
    //localStorage.removeItem("identity");
    //localStorage.removeItem("token");
    localStorage.clear();

    //* REDIRECCIÓN A INICIO
    this._router.navigate(["/login"]);
  }
}
