import { NgModule } from "@angular/core";

import { NzAvatarModule } from "ng-zorro-antd/avatar";
import { NzBreadCrumbModule } from "ng-zorro-antd/breadcrumb";
import { NzTransButtonModule } from "ng-zorro-antd/core/trans-button";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzI18nModule } from "ng-zorro-antd/i18n";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzImageModule } from "ng-zorro-antd/image";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzInputNumberModule } from "ng-zorro-antd/input-number";
import { NzModalModule } from "ng-zorro-antd/modal";
import { NzTabsModule } from "ng-zorro-antd/tabs";
import { NzToolTipModule } from "ng-zorro-antd/tooltip";
import { NzTypographyModule } from "ng-zorro-antd/typography";
import { NzUploadModule } from "ng-zorro-antd/upload";
import { NzResizableModule } from "ng-zorro-antd/resizable";
import { NzSwitchModule } from "ng-zorro-antd/switch";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NzDividerModule } from "ng-zorro-antd/divider";
import { NzCommentModule } from "ng-zorro-antd/comment";
import { NzPipesModule } from "ng-zorro-antd/pipes";
import { NzBadgeModule } from "ng-zorro-antd/badge";
import { NzCollapseModule } from "ng-zorro-antd/collapse";

@NgModule({
  exports: [
    NzAvatarModule,
    NzBreadCrumbModule,
    NzFormModule,
    NzI18nModule,
    NzIconModule,
    NzImageModule,
    NzInputModule,
    NzInputNumberModule,
    NzModalModule,
    NzTabsModule,
    NzToolTipModule,
    NzTransButtonModule,
    NzTypographyModule,
    NzUploadModule,
    NzResizableModule,
    NzSwitchModule,
    NzCardModule,
    NzGridModule,
    NzDividerModule,
    NzCommentModule,
    NzPipesModule,
    NzBadgeModule,
    NzCollapseModule,
  ],
})
export class ngZorroAntdModule {}
