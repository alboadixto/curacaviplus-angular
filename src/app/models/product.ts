export class Product {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public price?: number,
    public stock?: number,
    public is_active?: number,
    public company_id?: number,
    public image?: string
  ) {}
}
