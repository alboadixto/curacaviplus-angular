export class Company {
  constructor(
    //* TABLE COMPANIES
    public id?: number,
    public name?: string,
    public description?: string,

    //* TABLE IMAGES
    public image?: string,

    //* TABLE ADDRESSES
    public calle?: string,
    public numero?: number,
    public villa?: string,
    public referencia?: string,
    public phone?: string,
    public horario_ini?: string,
    public horario_fin?: string,
    public valor_envio?: number,
    public is_active?: number
  ) {}
}
