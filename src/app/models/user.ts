export class User {
  constructor(
    public id: number,
    public name: string,
    public email: string,
    public password: string,
    public confirmPassword: string,
    public role: string,
    public is_active: number,
    public img: string
  ) {}
}
