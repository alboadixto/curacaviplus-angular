import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "./auth/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { ForgotPasswordComponent } from "./auth/forgot-password/forgot-password.component";

import { IdentityGuard } from "./services/identity.guard";

const routes: Routes = [
  { path: "", redirectTo: "company", pathMatch: "full" },

  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [IdentityGuard],
  },

  { path: "login", component: LoginComponent },

  { path: "register", component: RegisterComponent },

  { path: "forgot-password", component: ForgotPasswordComponent },

  {
    path: "user",
    loadChildren: () => import("./user/user.module").then((m) => m.UserModule),
    canLoad: [IdentityGuard],
  },

  {
    path: "company",
    loadChildren: () =>
      import("./company/company.module").then((m) => m.CompanyModule),
    canLoad: [IdentityGuard],
  },

  { path: "**", redirectTo: "company", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
